create table role (id bigint not null auto_increment, role varchar(255), primary key (id)) type=MyISAM
create table user (id bigint not null auto_increment, date datetime, email varchar(255) not null, first_name varchar(255) not null, last_name varchar(255) not null, password varchar(255) not null, phone_number varchar(255) not null, user_name varchar(255) not null, primary key (id)) type=MyISAM
create table user_role (user_id bigint not null, role_id bigint not null, primary key (user_id, role_id)) type=MyISAM
alter table user_role add constraint FKa68196081fvovjhkek5m97n3y foreign key (role_id) references role (id)
alter table user_role add constraint FK859n2jvi8ivhui0rl0esws6o foreign key (user_id) references user (id)
