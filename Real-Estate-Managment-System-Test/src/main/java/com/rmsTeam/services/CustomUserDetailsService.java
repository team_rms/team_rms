package com.rmsTeam.services;

import java.util.Arrays;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.rmsTeam.models.CustomUserDetails;
import com.rmsTeam.models.Role;
import com.rmsTeam.models.User;
import com.rmsTeam.repositories.RoleRepository;
import com.rmsTeam.repositories.UserRepository;

@Service
public class CustomUserDetailsService implements UserDetailsService
{
	private UserRepository userRepository;
    private RoleRepository roleRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public CustomUserDetailsService(UserRepository userRepository, RoleRepository roleRepository, BCryptPasswordEncoder bCryptPasswordEncoder) 
    {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }
    
    
    public User saveUser(User user) 
    {	
    	user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
    	Role userRole = roleRepository.findByRole("Customer");
    	
        user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
        userRepository.save(user);
        return user;
    }
    
    public void deleteUser(long id)
    {
    	userRepository.deleteById(id);
    }
    
    /*
    public User saveUser(User user, String userType) 
    {
    	Role userRole;
    	user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
    	if(userType.equals("NormalUser"))
    	{
    		userRole = roleRepository.findByRole("NormalUser");
    	}
    	else if(userType.equals("CompanyOwnerUser"))
    	{
    		userRole = roleRepository.findByRole("CompanyOwner");
    	}
    	else
    	{
    		userRole = roleRepository.findByRole("Undefined");
    	}
        user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
        userRepository.save(user);
        return user;
    }*/

    public User findUserByUsername(String username) 
    {
    	return userRepository.findByUserName(username);
    }
    
    public User findUserByCompanyName(String companyName) 
    {
    	return userRepository.findByCompanyName(companyName);
    }
    
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException 
	{
		
		User user = userRepository.findByUserName(username);
		
		if(user != null) {
			return new CustomUserDetails(user);
		}
		throw new UsernameNotFoundException("User '" + username + "' not found");
	}

}
