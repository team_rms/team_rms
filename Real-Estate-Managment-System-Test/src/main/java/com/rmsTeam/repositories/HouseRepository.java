package com.rmsTeam.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.rmsTeam.models.House;

@Repository
public interface HouseRepository extends CrudRepository<House, Long>
{
	@Query(value = "SELECT * FROM house r WHERE LOWER(r.name) LIKE LOWER(CONCAT('%',:name, '%')) ", nativeQuery = true)
	List<House> searchByName(String name);
	
	//SELECT * FROM real_estate WHERE location LIKE '% %' AND price > '0' AND price < '1000000000' AND number_of_bed_rooms = '1'
	@Query(value = "SELECT * FROM house r WHERE " +
            "LOWER(r.location) LIKE LOWER(CONCAT('%',:location, '%')) AND " +
            "r.price >= :minPrice AND r.price < :maxPrice AND " +
            "r.area >= :minArea AND r.area < :maxArea AND r.number_of_bed_rooms = :numberOfBedRooms AND " + 
            "LOWER(r.type) LIKE LOWER(CONCAT('%',:type, '%')) AND r.active = 1",
            nativeQuery = true
    )
	List<House> findBySearchFilter(@Param(value = "location") String location,@Param("minPrice") int minPrice, 
										@Param("maxPrice") int maxPrice, @Param("numberOfBedRooms") int numberOfBedRooms,
										@Param("minArea") int minArea, @Param("maxArea") int maxArea, @Param("type") String type);
}
