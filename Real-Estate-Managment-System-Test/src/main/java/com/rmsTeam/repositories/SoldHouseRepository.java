package com.rmsTeam.repositories;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import com.rmsTeam.models.SoldHouse;

public interface SoldHouseRepository extends CrudRepository<SoldHouse, Long>
{
	List<SoldHouse> findAllByUserId(long userId);
	List<SoldHouse> findAllByHouseId(long houseId);
}
