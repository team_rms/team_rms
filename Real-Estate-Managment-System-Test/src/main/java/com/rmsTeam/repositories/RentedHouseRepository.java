package com.rmsTeam.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import com.rmsTeam.models.RentedHouse;

public interface RentedHouseRepository extends CrudRepository<RentedHouse, Long>
{
	List<RentedHouse> findAllByUserId(long userId);
	List<RentedHouse> findAllByHouseId(long houseId);
	List<RentedHouse> findAllByActive(boolean isActive);
}
