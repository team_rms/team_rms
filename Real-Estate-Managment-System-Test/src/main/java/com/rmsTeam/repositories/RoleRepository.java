package com.rmsTeam.repositories;

import org.springframework.data.repository.CrudRepository;

import com.rmsTeam.models.Role;

public interface RoleRepository extends CrudRepository<Role, Long>
{
	 Role findByRole(String role);
}
