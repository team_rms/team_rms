package com.rmsTeam.controllers;

import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@EnableGlobalMethodSecurity(prePostEnabled = true)
@Controller
@RequestMapping("/index")

public class indexController 
{
	//@PreAuthorize("hasAnyRole('ADMIN')")
	@GetMapping
	public String showLogInForm(Model model)
	{
		return "index";
	}

}
