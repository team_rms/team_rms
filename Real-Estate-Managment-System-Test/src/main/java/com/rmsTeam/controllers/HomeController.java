package com.rmsTeam.controllers;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.rmsTeam.models.House;
import com.rmsTeam.models.RentedHouse;
import com.rmsTeam.models.SoldHouse;
import com.rmsTeam.repositories.HouseRepository;
import com.rmsTeam.repositories.RentedHouseRepository;
import com.rmsTeam.repositories.SoldHouseRepository;
import com.rmsTeam.repositories.UserRepository;

@Controller
@RequestMapping("/home")
public class HomeController 
{
	
	private HouseRepository houseRepository;
	private RentedHouseRepository rentedHouseRepository;
	private SoldHouseRepository soldHouseRepository;
	private UserRepository userRepository;
	
	@Autowired
	public HomeController(HouseRepository realEstateRepository, RentedHouseRepository rentedHouseRepository, SoldHouseRepository soldHouseRepository, UserRepository userRepository)
	{
		this.houseRepository = realEstateRepository;
		this.rentedHouseRepository =  rentedHouseRepository;
		this.soldHouseRepository = soldHouseRepository;
		this.userRepository = userRepository;
	}
	
	@GetMapping("/selectHome")
	public String selectHomePage(Model model)
	{
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		if(auth.getAuthorities().toArray()[0].toString().equals("ROLE_ANONYMOUS"))
		{
			return "redirect:/login";
		}
		else if(auth.getAuthorities().toArray()[0].toString().equals("ROLE_Customer"))
		{
			return "redirect:/home";
		}
		else if(auth.getAuthorities().toArray()[0].toString().equals("ROLE_ADMIN"))
		{
			return "redirect:/adminHome";
		}
		else
		{
			return "redirect:/login";
		}
	}
	
	@GetMapping
	public String showHomePage(Model model)
	{
		addAttributesToModel(model);
		model.addAttribute("searched", false);
		return "home";
	}
	
	@GetMapping("viewHouse/{id}")
	public String showDetailHouse(@PathVariable("id") long id, Model model)
	{
		House house = houseRepository.findById(id).get();
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		long currentLoggedInUserId = userRepository.findByUserName(auth.getName()).getId();
		
		if(house.getType().equals("For Sell"))
		{
			boolean boughtHouseByCurrentUser = false;
			boolean boughtHouse = false;
			List<SoldHouse> soldHouses = soldHouseRepository.findAllByHouseId(id);
			
			if(soldHouses.size() != 0)
			{
				for(SoldHouse soldHouse : soldHouses)
				{
					if(soldHouse.getUserId() == currentLoggedInUserId)
					{
						boughtHouseByCurrentUser = true;
					}
				}
				
				if(!boughtHouseByCurrentUser)
				{
					boughtHouse = true;
				}
			}
			else
			{
				boughtHouse = true;
			}
			if(boughtHouse)
			{
				model.addAttribute("requestedBefore", false);
			}
			else
			{
				model.addAttribute("requestedBefore", true);
			}
			model.addAttribute("forSell", true);
		}
		else
		{
			boolean rentedHouseByCurrentUser = false;
			boolean rentHouse = false;
			List<RentedHouse> rentedHouses = rentedHouseRepository.findAllByHouseId(id);
			
			if(rentedHouses.size() != 0)
			{
				for(RentedHouse rentedHouse : rentedHouses)
				{
					if(rentedHouse.getUserId() == currentLoggedInUserId)
					{
						rentedHouseByCurrentUser = true;
					}
				}
				
				if(!rentedHouseByCurrentUser)
				{
					rentHouse = true;
				}
			}
			else
			{
				rentHouse = true;
			}
			if(rentHouse)
			{
				model.addAttribute("requestedBefore", false);
			}
			else
			{
				model.addAttribute("requestedBefore", true);
			}
			model.addAttribute("forSell", false);
		}
		
		addAttributesToModel(model);
		model.addAttribute("house", house);
		return "viewHouse";
	}
	
	@GetMapping("/searchHouseByName")
	public String searchHouseByName(@RequestParam(value="name", required = false) String name, Model model)
	{
		List<House> houses = houseRepository.searchByName(name);
		
		if(houses.size() == 0)
		{
			model.addAttribute("noRealEstate", true);
		}
		else
		{
			model.addAttribute("noRealEstate", false);
			model.addAttribute("realEstates", houses);
		}
		
		model.addAttribute("houseCountForNonLoggedInUser", houses.size());
		model.addAttribute("searched", true);
		addAttributesToModel(model);
		
		return "home";
	}
	
	@PostMapping("/searchHouse")
	public String searchHouse(@RequestParam(value="location", required = false) String location,
							  @RequestParam(value="priceRange", required = false) String priceRange,
							  @RequestParam(value="numberOfBedRooms", required = false) String numberOfBedRooms,
							  @RequestParam(value="area", required = false) String area,
							  @RequestParam(value="type", required = false) String type,
							  Model model)
	{
		List<House> finalRealEstates = new ArrayList<House>();
		
		String finalLocation;
		String finalType;
		int minPrice;
		int maxPrice;
		int finalNumberOfBedRooms;
		int minArea;
		int maxArea;
		
		if(!location.equals("") && location != null)
		{
			if(location.equals("Any Where"))
			{
				finalLocation = "";
			}
			else
			{
				finalLocation = location;
			}
		}
		else
		{
			finalLocation = "";
		}
		
		if(!priceRange.equals("") && priceRange != null)
		{
			switch(priceRange)
			{
				case "Any Price":
					minPrice = 0;
					maxPrice = Integer.MAX_VALUE;
					break;
				case "Less than 1,000,000":
					minPrice = 0;
					maxPrice = 1000000;
					break;
				case "1,000,000 - 3,000,000":
					minPrice = 1000000;
					maxPrice = 3000000;
					break;
				case "3,000,000 - 5,000,000":
					minPrice = 3000000;
					maxPrice = 5000000;
					break;
				case "5,000,000 - 10,000,000":
					minPrice = 5000000;
					maxPrice = 10000000;
					break;
				case "10,000,000 - 15,000,000":
					minPrice = 10000000;
					maxPrice = 15000000;
					break;
				default:
					minPrice = 0;
					maxPrice = Integer.MAX_VALUE;
					break;
			}
		}
		else
		{
			minPrice = 0;
			maxPrice = Integer.MAX_VALUE;
		}
		
		if(!numberOfBedRooms.equals("") && numberOfBedRooms != null)
		{
			finalNumberOfBedRooms = Integer.parseInt(numberOfBedRooms);
		}
		else
		{
			finalNumberOfBedRooms = 1;
		}
		
		if(!area.equals("") && area != null)
		{
			switch(area)
			{
				case "less than 70":
					minArea = 0;
					maxArea = 70;
					break;
				case "70 - 150":
					minArea = 70;
					maxArea = 150;
					break;
				case "150 - 300":
					minArea = 150;
					maxArea = 300;
					break;
				case "300 - 500":
					minArea = 300;
					maxArea = 500;
					break;
				case "500 - 1000":
					minArea = 500;
					maxArea = 1000;
					break;
				case "More than 1000":
					minArea = 1000;
					maxArea = Integer.MAX_VALUE;
					break;
				default:
					minArea = 0;
					maxArea = Integer.MAX_VALUE;
					break;
			}
		}
		else
		{
			minArea = 0;
			maxArea = Integer.MAX_VALUE;
		}
		
		if(!type.equals("") && type != null)
		{
			if(type.equals("Any Type"))
			{
				finalType = "";
			}
			else
			{
				finalType = type;
			}
		}
		else
		{
			finalType = "";
		}
		
		System.out.println("type " + finalType);

		finalRealEstates.addAll(houseRepository.findBySearchFilter(finalLocation, minPrice, maxPrice, finalNumberOfBedRooms, minArea, maxArea, finalType));
		
		if(finalRealEstates.size() == 0)
		{
			System.out.println("nothing found");
			model.addAttribute("noRealEstate", true);
		}
		else
		{
			model.addAttribute("noRealEstate", false);
			model.addAttribute("realEstates", finalRealEstates);
		}
		
		model.addAttribute("houseCountForNonLoggedInUser", finalRealEstates.size());
		model.addAttribute("searched", true);
		addAttributesToModel(model);
		
		return "/home";
	}
	
	@GetMapping("/rent")
	public String rent(@RequestParam(value = "houseId", required = false) long houseId)
	{
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		long currentLoggedInUserId = userRepository.findByUserName(auth.getName()).getId();
		//House house = houseRepository.findById(houseId).get();
		
		boolean rentedHouseByCurrentUser = false;
		boolean rentHouse = false;
		
		List<RentedHouse> rentedHouses = rentedHouseRepository.findAllByHouseId(houseId);
		
		if(rentedHouses.size() != 0)
		{
			for(RentedHouse reservation : rentedHouses)
			{
				if(reservation.getUserId() == currentLoggedInUserId)
				{
					rentedHouseByCurrentUser = true;
				}
			}
			
			if(!rentedHouseByCurrentUser)
			{
				rentHouse = true;
			}
		}
		else
		{
			rentHouse = true;
		}
		
		if(rentHouse)
		{
			RentedHouse rentedHouse = new RentedHouse();
			rentedHouse.setUserId(currentLoggedInUserId);
			rentedHouse.setHouseId(houseId);
			rentedHouse.setActive(false);
			
			rentedHouseRepository.save(rentedHouse);
		}
		
		return "redirect:/home/viewHouse/" + houseId;
	}
	
	@GetMapping("buy")
	public String buy(@RequestParam(value = "houseId") long houseId)
	{
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		long currentLoggedInUserId = userRepository.findByUserName(auth.getName()).getId();
		//House house = houseRepository.findById(houseId).get();
		
		boolean boughtHouseByCurrentUser = false;
		boolean boughtHouse = false;
		
		List<SoldHouse> soldHouses = soldHouseRepository.findAllByHouseId(houseId);
		
		if(soldHouses.size() != 0)
		{
			for(SoldHouse soldHouse : soldHouses)
			{
				if(soldHouse.getUserId() == currentLoggedInUserId)
				{
					boughtHouseByCurrentUser = true;
				}
			}
			
			if(!boughtHouseByCurrentUser)
			{
				boughtHouse = true;
			}
		}
		else
		{
			boughtHouse = true;
		}
		if(boughtHouse)
		{
			SoldHouse soledHouse = new SoldHouse();
			soledHouse.setUserId(currentLoggedInUserId);
			soledHouse.setHouseId(houseId);
			soledHouse.setActive(false);
			
			soldHouseRepository.save(soledHouse);
		}
		
		return "redirect:/home/viewHouse/" + houseId;
	}
	
	private void addAttributesToModel(Model model)
	{
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		if(auth.getAuthorities().toArray()[0].toString().equals("ROLE_ANONYMOUS"))
		{
			model.addAttribute("isLoggedIn", false);
			System.out.println(auth.getName());
		}
		else if(auth.getAuthorities().toArray()[0].toString().equals("ROLE_Customer"))
		{
			model.addAttribute("isLoggedIn", true);
			model.addAttribute("userName", auth.getName());
		}
		else if(auth.getAuthorities().toArray()[0].toString().equals("ROLE_ADMIN"))
		{
			model.addAttribute("isLoggedIn", true);
			model.addAttribute("userName", auth.getName());
		}
	}
	//@RequestParam(value="category", required = false) String category,
}
