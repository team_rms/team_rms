package com.rmsTeam.controllers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.rmsTeam.models.House;
import com.rmsTeam.models.RentedHouse;
import com.rmsTeam.models.RentedHouseViewModel;
import com.rmsTeam.models.SoldHouse;
import com.rmsTeam.models.SoldHouseViewModel;
import com.rmsTeam.models.User;
import com.rmsTeam.repositories.HouseRepository;
import com.rmsTeam.repositories.RentedHouseRepository;
import com.rmsTeam.repositories.SoldHouseRepository;
import com.rmsTeam.repositories.UserRepository;

@Controller
@RequestMapping("/adminHome")
public class AdminHomeController 
{
	private HouseRepository houseRepository;
	private UserRepository userRepository;
	private RentedHouseRepository rentedHouseRepository;
	private SoldHouseRepository soldHouseRepository;
	
	@Autowired
	public AdminHomeController(HouseRepository houseRepository, UserRepository userRepository, RentedHouseRepository renedHouseRepository, 
								SoldHouseRepository soldHouseRepository)
	{
		this.houseRepository = houseRepository;
		this.userRepository = userRepository;
		this.rentedHouseRepository = renedHouseRepository;
		this.soldHouseRepository = soldHouseRepository;
	}
	
	@GetMapping
    public String showAdminPage(Model model) 
    {
		Iterable<House> houses = houseRepository.findAll();
		model.addAttribute("houses", houses);
		return "adminHome";
    }
	
	@GetMapping("/addNewHouse")
    public String showHouseForm(Model model)
    {
		model.addAttribute("house", new House());
		return "addNewHouse";
    }
	
	@GetMapping("/allUsers")
    public String showAllUsers(Model model)
    {
		List<User> users = (List<User>)userRepository.findAll();
		model.addAttribute("customers", users);
		return "allUsers";
    }
	
	@GetMapping("/edit/{id}")
    public String editHouse(@PathVariable("id") long id, Model model)
    {
		model.addAttribute("house", houseRepository.findById(id).get());
		return "editHouse";
    }
	
	@GetMapping("rentRequests")
    public String showRentRequests(Model model) 
    {	
		List<RentedHouse> rentedHouses = (List<RentedHouse>) rentedHouseRepository.findAllByActive(false);
		List<RentedHouseViewModel> rentedHouseViewModels = new ArrayList<RentedHouseViewModel>();
		
		if(rentedHouses.size() != 0)
		{
			for(RentedHouse rentedHouse : rentedHouses)
			{
				String houseName = houseRepository.findById(rentedHouse.getHouseId()).get().getName();
				String fullName = userRepository.findById(rentedHouse.getUserId()).get().getFirstName() + " " + userRepository.findById(rentedHouse.getUserId()).get().getLastName();
				String email = userRepository.findById(rentedHouse.getUserId()).get().getEmail();
				String phone = userRepository.findById(rentedHouse.getUserId()).get().getPhoneNumber();
				String location = houseRepository.findById(rentedHouse.getHouseId()).get().getLocation();
				String imageName = houseRepository.findById(rentedHouse.getHouseId()).get().getHouseImage();
				
				rentedHouseViewModels.add(new RentedHouseViewModel(rentedHouse, fullName, phone, email, houseName, location, imageName));
			}
		}
		model.addAttribute("rentedHouses", rentedHouseViewModels);
    	return "rentRequests";
    }
	
	@GetMapping("buyRequests")
    public String showBuyRequests(Model model) 
    {	
		List<SoldHouse> soldHouses = (List<SoldHouse>) soldHouseRepository.findAll();
		List<SoldHouseViewModel> soldHouseViewModels = new ArrayList<SoldHouseViewModel>();
		
		if(soldHouses.size() != 0)
		{
			for(SoldHouse soldHouse : soldHouses)
			{
				String houseName = houseRepository.findById(soldHouse.getHouseId()).get().getName();
				String fullName = userRepository.findById(soldHouse.getUserId()).get().getFirstName() + " " + userRepository.findById(soldHouse.getUserId()).get().getLastName();
				String email = userRepository.findById(soldHouse.getUserId()).get().getEmail();
				String phone = userRepository.findById(soldHouse.getUserId()).get().getPhoneNumber();
				String location = houseRepository.findById(soldHouse.getHouseId()).get().getLocation();
				String imageName = houseRepository.findById(soldHouse.getHouseId()).get().getHouseImage();
				
				soldHouseViewModels.add(new SoldHouseViewModel(soldHouse, fullName, phone, email, houseName, location, imageName));
			}
		}
		
		model.addAttribute("soldHouses", soldHouseViewModels);
    	return "buyRequests";
    }
	
	@GetMapping("leaseRent")
    public String showActiveRequests(Model model) 
    {	
		List<RentedHouse> rentedHouses = (List<RentedHouse>) rentedHouseRepository.findAllByActive(true);
		List<RentedHouseViewModel> rentedHouseViewModels = new ArrayList<RentedHouseViewModel>();
		
		if(rentedHouses.size() != 0)
		{
			for(RentedHouse rentedHouse : rentedHouses)
			{
				String houseName = houseRepository.findById(rentedHouse.getHouseId()).get().getName();
				String fullName = userRepository.findById(rentedHouse.getUserId()).get().getFirstName() + " " + userRepository.findById(rentedHouse.getUserId()).get().getLastName();
				String email = userRepository.findById(rentedHouse.getUserId()).get().getEmail();
				String phone = userRepository.findById(rentedHouse.getUserId()).get().getPhoneNumber();
				String location = houseRepository.findById(rentedHouse.getHouseId()).get().getLocation();
				String imageName = houseRepository.findById(rentedHouse.getHouseId()).get().getHouseImage();
				
				rentedHouseViewModels.add(new RentedHouseViewModel(rentedHouse, fullName, phone, email, houseName, location, imageName));
			}
		}
		model.addAttribute("rentedHouses", rentedHouseViewModels);
    	return "leaseRent";
    }
	
	@GetMapping("deleteHouse/{id}")
    public String deleteHouse(@PathVariable("id") long id) 
    {	
    	houseRepository.deleteById(id);
    	return "redirect:/adminHome";
    }
    
    @GetMapping("deleteCustomer/{id}")
    public String deleteCustomer(@PathVariable("id") long id) 
    {	
    	User u = userRepository.findById(id).get();
    	u.getRoles().clear();
    	userRepository.save(u);
    	userRepository.deleteById(id);
    	return "redirect:/adminHome/allUsers";
    }
    
    @GetMapping("discardRentRequest/{id}")
    public String deleteRentRequest(@PathVariable("id") long id) 
    {	
    	rentedHouseRepository.deleteById(id);
   
    	return "redirect:/adminHome/rentRequests";
    }
    
    @GetMapping("acceptRentRequest/{id}")
    public String acceptRentRequest(@PathVariable("id") long id) 
    {	
    	RentedHouse rentedHouse = rentedHouseRepository.findById(id).get();
    	House house = houseRepository.findById(rentedHouse.getHouseId()).get();
    	long houseId = house.getId();
    	house.setActive(false);
    	List<RentedHouse> allRented = rentedHouseRepository.findAllByHouseId(houseId);
    	
    	for(RentedHouse houseToDelete: allRented)
    	{
    		
    		long toDeleteId = houseToDelete.getId();
    		if(toDeleteId != id)
    		{
    			rentedHouseRepository.deleteById(toDeleteId);
    		}
    		
    	}
    	
    	rentedHouse.setActive(true);
    	
    	houseRepository.save(house);
    	rentedHouseRepository.save(rentedHouse);
    	
    	return "redirect:/adminHome/rentRequests";
    }
    
    @GetMapping("discardBuyRequest/{id}")
    public String deleteBuyRequest(@PathVariable("id") long id) 
    {	
    	soldHouseRepository.deleteById(id);
    	
    	return "redirect:/adminHome/buyRequests";
    }
    
    @GetMapping("acceptBuyRequest/{id}")
    public String acceptBuyRequest(@PathVariable("id") long id) 
    {	
    	SoldHouse soldHouse = soldHouseRepository.findById(id).get();
    	House house = houseRepository.findById(soldHouse.getHouseId()).get();
    	long houseId = house.getId();
    	house.setActive(false);
    	soldHouse.setActive(true);
    	List<SoldHouse> allSold = soldHouseRepository.findAllByHouseId(houseId);
    	
    	for(SoldHouse houseToDelete: allSold)
    	{
    		
    		long toDeleteId = houseToDelete.getId();
    		soldHouseRepository.deleteById(toDeleteId);
    		
    	}
    	houseRepository.save(house);
    	soldHouseRepository.save(soldHouse);
    	
    	return "redirect:/adminHome/buyRequests";
    }
    
    @GetMapping("leaseRent/{id}")
    public String leaseRent(@PathVariable("id") long id) 
    {	
    	RentedHouse rentedHouse = rentedHouseRepository.findById(id).get();
    	House house = houseRepository.findById(rentedHouse.getHouseId()).get();
    	house.setActive(true);
    	
    	houseRepository.save(house);
    	rentedHouseRepository.delete(rentedHouse);
    	
    	return "redirect:/adminHome/leaseRent";
    }
    
    @PostMapping("add")
    public String processHouseForm(@Valid @ModelAttribute("house") House house, @RequestParam("houseImageFile") MultipartFile imageFile,  Errors errors, Model model) 
    {
    	if (errors.hasErrors()) 
    	{
    		return "addHouseForm";
        }
    	
    	String folder = "/home/robi/Documents/workspace-spring-tool-suite-4-4.0.1.RELEASE/Real-Estate-Managment-System-Test/src/main/resources/static/images/";
    	try 
    	{
			byte[] bytes = imageFile.getBytes();
			Path path = Paths.get(folder + imageFile.getOriginalFilename());
			Files.write(path, bytes);
			
		} catch (IOException e) 
    	{
			e.printStackTrace();
		}
    	
    	house.setHouseImage(imageFile.getOriginalFilename());
    	house.setActive(true);
    	
    	houseRepository.save(house);
    	
    	return "redirect:/adminHome";
    }
    
    @PostMapping("edit")
    public String updateRealEstate(@Valid @ModelAttribute("house") House house, @RequestParam("houseImageFile") MultipartFile imageFile, Errors errors, Model model) 
    {
    	if (errors.hasErrors()) 
    	{
    		return "editHouse";
        }
    	House houseFromDB = houseRepository.findById(house.getId()).get();
    	
    	if(house.getName() != null)
    	{
    		houseFromDB.setName(house.getName());
    	}
    	
    	if(house.getNumberOfBedRooms() >= 1)
    	{
    		houseFromDB.setNumberOfBedRooms(house.getNumberOfBedRooms());
    	}
    	
    	if(house.getArea() != null)
    	{
    		houseFromDB.setArea(house.getArea());
    	}
    	
    	if(house.getLocation() != null)
    	{
    		houseFromDB.setLocation(house.getLocation());
    	}
    	
    	if(house.getPrice() > 1000)
    	{
    		houseFromDB.setPrice(house.getPrice());
    	}
    	
    	if(imageFile.getOriginalFilename() != null && !imageFile.getOriginalFilename().equals(houseFromDB.getHouseImage()))
    	{
    		String folder = "/home/robi/Documents/workspace-spring-tool-suite-4-4.0.1.RELEASE/Real-Estate-Managment-System-Test/src/main/resources/static/images/";
        	try 
        	{
    			byte[] bytes = imageFile.getBytes();
    			Path path = Paths.get(folder + imageFile.getOriginalFilename());
    			Files.write(path, bytes);
    			
    		} catch (IOException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    		houseFromDB.setHouseImage(imageFile.getOriginalFilename());
    	}
    	
    	houseRepository.save(houseFromDB);
    	
    	return "redirect:/adminHome";
    }
}
