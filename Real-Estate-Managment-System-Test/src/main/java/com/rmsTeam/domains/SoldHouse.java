package com.rmsTeam.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "SoldHouse")
public class SoldHouse 
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private long id;
	
	@Column(name="userId")
	private long userId;

	@Column(name="houseId")
	private long houseId;
	
	@Column(name="active")
	private boolean active;
	
	@Column(name="selledDate")
	private Date selledDate;
	
	@PrePersist
	void rentedDate()
	{
		this.selledDate = new Date();
	}
}
