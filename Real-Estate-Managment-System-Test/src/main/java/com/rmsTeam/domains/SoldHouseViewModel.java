package com.rmsTeam.models;
import lombok.Data;

@Data
public class SoldHouseViewModel 
{
	private SoldHouse soldHouse;
	private String fullName;
	private String houseName;
	private String location;
	private String phone;
	private String email;
	private String imageName;
	
	public SoldHouseViewModel(SoldHouse soldHouse, String fullName, String phone, String email, String houseName, String location, String imageName)
	{
		this.soldHouse = soldHouse;
		this.fullName = fullName;
		this.phone = phone;
		this.email = email;
		this.houseName = houseName;
		this.location = location;
		this.imageName = imageName;
	}
}
