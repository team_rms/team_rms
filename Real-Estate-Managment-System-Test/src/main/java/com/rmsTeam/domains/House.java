package com.rmsTeam.models;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
@Entity
@Table(name = "House")
public class House 
{
	public House()
	{
		super();
	}
	
	public House(House house)
	{
		this.id = house.getId();
		this.name = house.getName();
		this.location = house.getLocation();
		this.numberOfBedRooms = house.getNumberOfBedRooms();
		this.area = house.getArea();
		this.price = house.getPrice();
		this.houseImage = house.getHouseImage();
		this.date = house.getDate();
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private long id;
	
	@NotNull
	@Size(min=1, message="name can't be empty")
	@Column(name="name")
	private String name;
	
	@NotNull
	@Column(name="numberOfBedRooms")
	private int numberOfBedRooms;
	
	@NotNull
	@Size(min=1, message="area can't be empty")
	@Column(name="area")
	private String area;
	
	@NotNull
	@Size(min=1, message="location can't be empty")
	@Column(name="location")
	private String location;
	
	@NotNull
	@Column(name="price")
	private int price;
	
	//@NotNull
	@Column(name="houseImage")
	private String houseImage;
	
	@Column(name="date")
	private Date date;
	
	@Column(name="active")
	private boolean active;
	
	//for sell or for rent
	@Column(name="type")
	private String type;
	
	@PrePersist
	void CreatedAt()
	{
		this.date = new Date();
	}

}
