package com.rmsTeam.services;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.rmsTeam.models.CustomUserDetails;
import com.rmsTeam.models.User;
import com.rmsTeam.repositories.UserRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomUserDetailsServiceTest 
{
	@Autowired
	private CustomUserDetailsService customUserDetailsService;
	
	@MockBean
	private UserRepository userRepository;
	
	private User getUser()
	{
		User user = new User();
		user.setFirstName("firstName");
		user.setLastName("lastName");
		user.setEmail("FL@gmail.com");
		user.setDate(new Date());
		user.setPhoneNumber("+215912345678");
		user.setUserName("firlas");
		user.setPassword("password");
		
		return user;
	}
	@Test
	public void testsaveUser()
	{
		User user = getUser();
		
		Mockito.when(userRepository.save(user)).thenReturn(user);
		
		assertThat(customUserDetailsService.saveUser(user)).isEqualTo(user);
	}
	
	@Test
	public void testfindUserByUsername()
	{
		User user = getUser();
		
		Mockito.when(userRepository.findByUserName("firlas")).thenReturn(user);
		
		assertThat(customUserDetailsService.findUserByUsername("firlas")).isEqualTo(user);
	}
	
	@Test
	public void testloadUserByUsername()
	{
		CustomUserDetails user = new CustomUserDetails(getUser());
		
		Mockito.when(userRepository.findByUserName("firlas")).thenReturn(user);
		
		assertThat(customUserDetailsService.loadUserByUsername("firlas")).isEqualTo(user);
	}
	
}
