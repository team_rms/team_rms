/*package com.rmsTeam.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.rmsTeam.models.*;
import com.rmsTeam.repositories.UserRepository;

@Controller
//@EnableJpaRepositories("com.rmsTeam.Repositories")
@RequestMapping("/login")
public class LoginController 
{	
	
	private UserRepository userRepository;
	
	@Autowired
	public LoginController(UserRepository userRepository)
	{
		this.userRepository = userRepository;
	}
	
	@GetMapping
    public String showDesignForm(Model model) 
    {
		//model.addAttribute("user", new User());
		//model.addAttribute("error", false);
		return "login";
    }

    @PostMapping
    public String processDesign(@Valid @ModelAttribute("user") User user, Errors errors, Model model) 
    {
    	if (errors.hasErrors()) 
    	{
    		return "login";
        }

    	if(!checkUserName(user) || !checkPassword(user))
    	{
    		//model.addAttribute("message", "Invalid username or password");
    		//model.addAttribute("error", true);
    		return "login";
    	}
    	
    	return "redirect:/index";
    }
    
    private boolean checkUserName(User user)
    {
    	List<User> users = (List<User>) userRepository.findAll();
    	
    	for(User userFromDataBase:users)
    	{
    		if(user.getUserName().contentEquals(userFromDataBase.getUserName()))
    		{
    			return true;
    		}
    	}
    	return false;
    }
    
    private boolean checkPassword(User user)
    {
    	List<User> users = (List<User>) userRepository.findAll();
    	
    	for(User userFromDataBase:users)
    	{
    		if(user.getPassword().contentEquals(userFromDataBase.getPassword()))
    		{
    			return true;
    		}
    	}
    	return false;
    }
}*/
