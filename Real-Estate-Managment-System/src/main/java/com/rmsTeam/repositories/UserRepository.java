package com.rmsTeam.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.rmsTeam.models.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> 
{
	User findByUserName(String userName);
}
