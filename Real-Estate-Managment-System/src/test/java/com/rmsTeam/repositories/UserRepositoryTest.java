package com.rmsTeam.repositories;



import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.rmsTeam.models.User;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest 
{
	@Autowired
	private TestEntityManager entityManager;
	
	@Autowired
	private UserRepository userRepository;
	
	private User getUser()
	{
		User user = new User();
		user.setFirstName("firstName");
		user.setLastName("lastName");
		user.setEmail("FL@gmail.com");
		user.setDate(new Date());
		user.setPhoneNumber("+215912345678");
		user.setUserName("firlas");
		user.setPassword("password");
		return user;
	}
	
	@Test
	public void testSaveUser()
	{
		User user = getUser();
		User savedInDB = entityManager.persist(user);
		
		User getFromDB = userRepository.findByUserName(savedInDB.getUserName());
		
		assertThat(getFromDB).isEqualTo(savedInDB);
	}
	
	@Test
	public void testGetAllUser()
	{
		User user = new User();
		user.setFirstName("firstName1");
		user.setLastName("lastName1");
		user.setEmail("FL@gmail1.com");
		user.setDate(new Date());
		user.setPhoneNumber("+215912345678");
		user.setUserName("firlas1");
		user.setPassword("password1");
		
		User user1 = new User();
		user1.setFirstName("firstName2");
		user1.setLastName("lastName2");
		user1.setEmail("FL@gmail2.com");
		user1.setDate(new Date());
		user1.setPhoneNumber("+215912345678");
		user1.setUserName("firlas2");
		user1.setPassword("password2");
		
		entityManager.persist(user);
		entityManager.persist(user1);
		
		Iterable<User> allUsers = userRepository.findAll();
		List<User> userList = new ArrayList<>();
		
		for(User user3: allUsers)
		{
			userList.add(user3);
		}
		
		assertThat(userList.size()).isEqualTo(2);
	}
	
	@Test
	public void testdeleteByUserName()
	{
		User user = new User();
		user.setFirstName("firstName1");
		user.setLastName("lastName1");
		user.setEmail("FL@gmail1.com");
		user.setDate(new Date());
		user.setPhoneNumber("+215912345678");
		user.setUserName("firlas1");
		user.setPassword("password1");
		
		User user1 = new User();
		user1.setFirstName("firstName2");
		user1.setLastName("lastName2");
		user1.setEmail("FL@gmail2.com");
		user1.setDate(new Date());
		user1.setPhoneNumber("+215912345678");
		user1.setUserName("firlas2");
		user1.setPassword("password2");
		
		entityManager.persist(user);
		User user3 = entityManager.persist(user1);
		
		entityManager.remove(user3);
		
		Iterable<User> allUsers = userRepository.findAll();
		List<User> userList = new ArrayList<>();
		
		for(User user2: allUsers)
		{
			userList.add(user2);
		}
		
		assertThat(userList.size()).isEqualTo(1);
	}
	
	@Test
	public void testfindByUserName()
	{
		User user = getUser();
		User savedInDB = entityManager.persist(user);
		
		User getFromDB = userRepository.findByUserName(savedInDB.getUserName());
		
		assertThat(getFromDB).isEqualTo(savedInDB);
	}
}
