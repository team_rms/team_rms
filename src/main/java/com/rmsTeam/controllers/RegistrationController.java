package com.rmsTeam.controllers;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.rmsTeam.models.*;
import com.rmsTeam.services.CustomUserDetailsService;

@Controller
@RequestMapping("/registration")
public class RegistrationController 
{
	private CustomUserDetailsService customUserDetailsService;
	
	@Autowired
	public RegistrationController(CustomUserDetailsService customUserDetailsService)
	{
		this.customUserDetailsService = customUserDetailsService;
	}
	
	@GetMapping
    public String showDesignForm(Model model) 
    {
		model.addAttribute("user", new User());
		model.addAttribute("error", false);
		return "registration";
    }

    @PostMapping
    public String processDesign(@Valid @ModelAttribute("user") User user, Errors errors, Model model) 
    {
    	if (errors.hasErrors()) 
    	{
    		return "registration";
        }

    	if(checkUserName(user))
    	{
    		model.addAttribute("message", "User name already exits");
    		model.addAttribute("error", true);
    		return "registration";
    	}
    	
    	customUserDetailsService.saveUser(user);
    	return "redirect:/index";
    }
    
    private boolean checkUserName(User user)
    {
    	User foundUser = customUserDetailsService.findUserByUsername(user.getUserName());
    	
    	if(foundUser != null)
    	{
    		return true;
    	}
    	return false;
    }
}
