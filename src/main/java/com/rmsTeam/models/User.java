package com.rmsTeam.models;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import javax.validation.constraints.Email;
import lombok.Data;

@Data
@Entity
@Table(name = "user")
public class User
{
	public User()
	{
		super();
	}
	
	public User(User user)
	{
		this.firstName = user.firstName;
		this.lastName = user.lastName;
		this.userName = user.userName;
		this.email = user.email;
		this.phoneNumber = user.phoneNumber;
		this.password = user.password;
		this.date = user.date;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private long id;
		
	@NotNull
	@Size(min=1, message="First Name can't be empty")
	@Column(name="firstName")
	private String firstName;
	
	@NotNull
	@Size(min=1, message="Last name can't be empty")
	@Column(name="lastName")
	private String lastName;
	
	@NotNull
	@Size(min=1, message="email can't be empty")
	@Email(message="Email format is not right")
	@Column(name="email")
	private String email;
	
	@NotNull
	@Size(min=1, message="phone number can't be empty")
	@Column(name="phoneNumber")
	private String phoneNumber;
	
	@NotNull
	@Size(min=3, message="User Name must be at least 3 characters long")
	@Column(name="userName")
	private String userName;
	
	@NotNull
	@Size(min=7, message="Password must be at least 7 characters long")
	@Column(name="password")
	private String password;
	
	@Column(name="date")
	private Date date;
	
	@PrePersist
	void CreatedAt()
	{
		this.date = new Date();
	}
	
	@ManyToMany(cascade = CascadeType.ALL, fetch=FetchType.EAGER)
    @JoinTable(name="user_role", 
    			joinColumns= {@JoinColumn(name="user_id")},
    			inverseJoinColumns= {@JoinColumn(name="role_id")})
    private Set<Role> roles;
}
