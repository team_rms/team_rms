package com.rmsTeam.models;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class CustomUserDetails extends User implements UserDetails 
{
	User user;
	public CustomUserDetails(final User user)
	{
		this.user = user;
	}
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() 
	{
		return user.getRoles()
					.stream()
					.map(role -> new SimpleGrantedAuthority("ROLE_" + role.getRole()))
					.collect(Collectors.toList());
	}

	@Override
	public String getUsername() 
	{
		return user.getUserName();
	}
	
	@Override
	public String getPassword() 
	{
		return user.getPassword();
	}

	@Override
	public boolean isAccountNonExpired() 
	{
		return true;
	}

	@Override
	public boolean isAccountNonLocked() 
	{
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() 
	{
		return true;
	}

	@Override
	public boolean isEnabled() 
	{
		return true;
	}

}
