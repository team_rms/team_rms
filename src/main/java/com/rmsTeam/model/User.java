package com.rmsTeam.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;

@Data
public class User 
{
	private String id;
	@NotNull
	@Size(min=3, message="User Name must be at least 3 characters long")
	private String name;
	@NotNull
	@Size(min=7, message="Password must be at least 7 characters long")
	private String password;
	
}
